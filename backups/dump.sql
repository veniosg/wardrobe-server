CREATE DATABASE  IF NOT EXISTS `wardrobe` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wardrobe`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: wardrobe
-- ------------------------------------------------------
-- Server version	5.6.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `collection`
--

DROP TABLE IF EXISTS `collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collection` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collection`
--

LOCK TABLES `collection` WRITE;
/*!40000 ALTER TABLE `collection` DISABLE KEYS */;
INSERT INTO `collection` VALUES (1,'Random Collection');
/*!40000 ALTER TABLE `collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collection_has_garment`
--

DROP TABLE IF EXISTS `collection_has_garment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collection_has_garment` (
  `Collection_id` bigint(20) NOT NULL,
  `Garment_id` bigint(20) NOT NULL,
  KEY `FK_fj16m0sx929vyious5hv6ivq5` (`Garment_id`),
  KEY `FK_b9hbcjqo0s8sxuc6fye40kd9o` (`Collection_id`),
  CONSTRAINT `FK_b9hbcjqo0s8sxuc6fye40kd9o` FOREIGN KEY (`Collection_id`) REFERENCES `collection` (`id`),
  CONSTRAINT `FK_fj16m0sx929vyious5hv6ivq5` FOREIGN KEY (`Garment_id`) REFERENCES `garment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collection_has_garment`
--

LOCK TABLES `collection_has_garment` WRITE;
/*!40000 ALTER TABLE `collection_has_garment` DISABLE KEYS */;
INSERT INTO `collection_has_garment` VALUES (1,6),(1,10),(1,3),(1,8),(1,7);
/*!40000 ALTER TABLE `collection_has_garment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `garment`
--

DROP TABLE IF EXISTS `garment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `garment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `recentlyPurchased` tinyint(1) NOT NULL DEFAULT '1',
  `weather` int(11) DEFAULT NULL,
  `Picture_id` bigint(20) DEFAULT NULL,
  `Type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_967kjfavkt3mjr11kjy7boe65` (`Picture_id`),
  KEY `FK_ljwd4ffjotajfllty3jhkx1k6` (`Type_id`),
  CONSTRAINT `FK_ljwd4ffjotajfllty3jhkx1k6` FOREIGN KEY (`Type_id`) REFERENCES `type` (`id`),
  CONSTRAINT `FK_967kjfavkt3mjr11kjy7boe65` FOREIGN KEY (`Picture_id`) REFERENCES `picture` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `garment`
--

LOCK TABLES `garment` WRITE;
/*!40000 ALTER TABLE `garment` DISABLE KEYS */;
INSERT INTO `garment` VALUES (1,'2013-11-27 20:55:16','Gray Uniqlo',0,0,1,6),(2,'2013-11-27 20:55:16','Light Gray Ambiguous',0,1,2,8),(3,'2013-11-27 20:55:16','Nixon Tee',0,1,3,4),(4,'2013-11-27 20:55:16','Blue Kr3w',0,0,4,7),(5,'2013-11-27 20:55:16','Beige Kr3w',0,3,5,1),(6,'2013-11-27 20:55:16','Burgundy Kr3w',0,3,6,1),(7,'2013-11-27 20:55:16','Lizard King Shirt',0,1,7,7),(8,'2013-11-27 20:55:16','Cult Tee',0,3,8,4),(9,'2013-11-27 20:55:16','Gray Etnies',0,3,9,3),(10,'2013-11-27 20:55:16','Black Supra',0,0,10,3),(11,'2013-12-09 10:52:07','McKinley Tee',0,2,11,4),(12,'2013-12-09 10:59:38','Red Shirt',0,1,12,7),(13,'2013-12-09 11:00:51','Kr3w Shirt',0,2,13,4),(14,'2013-12-09 11:02:22','Carhartt Pants',0,2,14,1),(15,'2013-12-09 11:03:05','Matix Tee',0,2,15,4),(16,'2013-12-09 11:03:46','Element Tee',0,2,16,4),(17,'2013-12-09 11:04:46','eS Tee',0,1,17,1),(18,'2013-12-09 11:08:25','Green Pants',0,0,18,1),(19,'2013-12-09 11:09:31','Circa Hoodie',0,0,19,8),(20,'2013-12-09 11:11:10','Plaid Shirt',0,0,20,7),(21,'2013-12-09 11:11:37','Carhartt Tee',0,3,21,4),(22,'2013-12-09 11:12:48','Kr3w Plaid Shirt',0,2,22,4),(23,'2013-12-09 11:13:25','Test Dress',0,1,23,2);
/*!40000 ALTER TABLE `garment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `garment_tags`
--

DROP TABLE IF EXISTS `garment_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `garment_tags` (
  `Garment_id` bigint(20) NOT NULL,
  `tags` varchar(255) DEFAULT NULL,
  KEY `FK_7a1bqtckxokly9mq5oc26yuim` (`Garment_id`),
  CONSTRAINT `FK_7a1bqtckxokly9mq5oc26yuim` FOREIGN KEY (`Garment_id`) REFERENCES `garment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `garment_tags`
--

LOCK TABLES `garment_tags` WRITE;
/*!40000 ALTER TABLE `garment_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `garment_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outfit`
--

DROP TABLE IF EXISTS `outfit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outfit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outfit`
--

LOCK TABLES `outfit` WRITE;
/*!40000 ALTER TABLE `outfit` DISABLE KEYS */;
/*!40000 ALTER TABLE `outfit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outfit_has_outfititem`
--

DROP TABLE IF EXISTS `outfit_has_outfititem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outfit_has_outfititem` (
  `Outfit_id` bigint(20) NOT NULL,
  `OutfitItem_id` bigint(20) NOT NULL,
  KEY `FK_iwos2pd7gj28blu5rtcv51xbr` (`OutfitItem_id`),
  KEY `FK_5cjhh8r1wgp769134orjtrsu5` (`Outfit_id`),
  CONSTRAINT `FK_5cjhh8r1wgp769134orjtrsu5` FOREIGN KEY (`Outfit_id`) REFERENCES `outfit` (`id`),
  CONSTRAINT `FK_iwos2pd7gj28blu5rtcv51xbr` FOREIGN KEY (`OutfitItem_id`) REFERENCES `outfititem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outfit_has_outfititem`
--

LOCK TABLES `outfit_has_outfititem` WRITE;
/*!40000 ALTER TABLE `outfit_has_outfititem` DISABLE KEYS */;
/*!40000 ALTER TABLE `outfit_has_outfititem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outfititem`
--

DROP TABLE IF EXISTS `outfititem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outfititem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rotation` float DEFAULT NULL,
  `scale` float DEFAULT NULL,
  `xCoord` float DEFAULT NULL,
  `yCoord` float DEFAULT NULL,
  `Garment_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_i03uxtujcri2hktii6kutwwf2` (`Garment_id`),
  CONSTRAINT `FK_i03uxtujcri2hktii6kutwwf2` FOREIGN KEY (`Garment_id`) REFERENCES `garment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outfititem`
--

LOCK TABLES `outfititem` WRITE;
/*!40000 ALTER TABLE `outfititem` DISABLE KEYS */;
/*!40000 ALTER TABLE `outfititem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `picture`
--

DROP TABLE IF EXISTS `picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `picture` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `color` int(11) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `picture`
--

LOCK TABLES `picture` WRITE;
/*!40000 ALTER TABLE `picture` DISABLE KEYS */;
INSERT INTO `picture` VALUES (1,-14344158,'http://192.168.0.101:8080/wardrobe/resources/images/1385377060833'),(2,-10069929,'http://192.168.0.101:8080/wardrobe/resources/images/1385377123913'),(3,-15133418,'http://192.168.0.101:8080/wardrobe/resources/images/1385377188083'),(4,-15461857,'http://192.168.0.101:8080/wardrobe/resources/images/1385377221957'),(5,-8358815,'http://192.168.0.101:8080/wardrobe/resources/images/1385377252017'),(6,-13691369,'http://192.168.0.101:8080/wardrobe/resources/images/1385377283432'),(7,-12631766,'http://192.168.0.101:8080/wardrobe/resources/images/1385377330106'),(8,-13620952,'http://192.168.0.101:8080/wardrobe/resources/images/1385377354566'),(9,-12698306,'http://192.168.0.101:8080/wardrobe/resources/images/1385377388152'),(10,-14935778,'http://192.168.0.101:8080/wardrobe/resources/images/1385377410198'),(11,-13867711,'http://192.168.0.101:8080/wardrobe/resources/images/1386585455530'),(12,-9943994,'http://192.168.0.101:8080/wardrobe/resources/images/1386585437966'),(13,-12303022,'http://192.168.0.101:8080/wardrobe/resources/images/1386585423225'),(14,-14278618,'http://192.168.0.101:8080/wardrobe/resources/images/1386585408034'),(15,-7896705,'http://192.168.0.101:8080/wardrobe/resources/images/1386585394928'),(16,-5066062,'http://192.168.0.101:8080/wardrobe/resources/images/1386585378449'),(17,-14262974,'http://192.168.0.101:8080/wardrobe/resources/images/1386585368880'),(18,-15394534,'http://192.168.0.101:8080/wardrobe/resources/images/1386585341961'),(19,-14343642,'http://192.168.0.101:8080/wardrobe/resources/images/1386585327776'),(20,-10265507,'http://192.168.0.101:8080/wardrobe/resources/images/1386585313720'),(21,-14343899,'http://192.168.0.101:8080/wardrobe/resources/images/1386585299026'),(22,-14213083,'http://192.168.0.101:8080/wardrobe/resources/images/1386585287837'),(23,-9808303,'http://192.168.0.101:8080/wardrobe/resources/images/1386585274601');
/*!40000 ALTER TABLE `picture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `allowedInstancesInOutfit` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (1,1,'Pants'),(2,1,'Dresses'),(3,1,'Shoes'),(4,1,'Basic tops'),(5,4,'Accessories'),(6,1,'Coats'),(7,1,'Other Tops'),(8,1,'Jackets');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-09 11:39:23
