package com.veniosg.wardrobe.repository;

import org.springframework.data.repository.CrudRepository;

import com.veniosg.wardrobe.entity.Outfit;

public interface OutfitRepository extends CrudRepository<Outfit, Long> {
}
