package com.veniosg.wardrobe.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.veniosg.wardrobe.entity.Garment;

public interface GarmentRepository extends CrudRepository<Garment, Long> {
	List<Garment> findByType_Id(Long typeId);
}
