package com.veniosg.wardrobe.repository;

import org.springframework.data.repository.CrudRepository;

import com.veniosg.wardrobe.entity.OutfitItem;

public interface OutfitItemRepository extends CrudRepository<OutfitItem, Long> {
}
