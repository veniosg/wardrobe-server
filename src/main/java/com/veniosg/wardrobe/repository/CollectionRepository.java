package com.veniosg.wardrobe.repository;

import org.springframework.data.repository.CrudRepository;

import com.veniosg.wardrobe.entity.Collection;

public interface CollectionRepository extends CrudRepository<Collection, Long> {
}
