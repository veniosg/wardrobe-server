package com.veniosg.wardrobe.repository;

import org.springframework.data.repository.CrudRepository;

import com.veniosg.wardrobe.entity.Type;

public interface TypeRepository extends CrudRepository<Type, Long> {
}
