package com.veniosg.wardrobe.repository;

import org.springframework.data.repository.CrudRepository;

import com.veniosg.wardrobe.entity.Picture;

public interface PictureRepository extends CrudRepository<Picture, Long> {
}
