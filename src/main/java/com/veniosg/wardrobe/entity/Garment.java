package com.veniosg.wardrobe.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Embeddable
public class Garment {
	private static final long NEW_THRESHOLD_MILLIS = 2 * (24*60*60*1000);
	
    private Long id;
    private Weather weather;
    private String name;
    private Picture picture;
    private Type type;
    private List<String> tags;
    private Boolean recentlyPurchased;
    
    private Date created;
    
    @PrePersist
    protected void onCreate() {
    	setCreated(new Date());
    }
    
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Enumerated(EnumType.ORDINAL)
	public Weather getWeather() {
		return weather;
	}

	public void setWeather(Weather weather) {
		this.weather = weather;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToOne(cascade = {CascadeType.ALL}, optional = true, orphanRemoval = true)
	@JoinColumn(name = "Picture_id")
	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Type_id")
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(joinColumns = @JoinColumn(name = "Garment_id"))
	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	@Column(nullable = false, columnDefinition = "boolean default true")
	public Boolean getRecentlyPurchased() {
		return recentlyPurchased;
	}

	public void setRecentlyPurchased(Boolean recentlyPurchased) {
		this.recentlyPurchased = recentlyPurchased;
	}

	/**
	 * Check if a garment is new.
	 * @return Whether this garment can be considered new or not; 
	 * (and possibly take precedence in the outfit generation).
	 */
	@Transient
	@JsonIgnore 
	public boolean isNew() {
		if (new Date().getTime() - getCreated().getTime() <= NEW_THRESHOLD_MILLIS
				&& getRecentlyPurchased()) {	// Only if recently bought
			return true;
		} else {
			return false;
		}
	}
	
	public static enum Weather {
		COLD, NORMAL, HOT, ANY
	}
}
