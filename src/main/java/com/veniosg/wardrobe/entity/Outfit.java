package com.veniosg.wardrobe.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class Outfit {
    private Long id;
    private List<OutfitItem> items;
    private String title;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.FALSE)		// See http://stackoverflow.com/questions/10769656/how-to-use-jpa-2-0-manytomany-without-issues
	@JoinTable(
			joinColumns = @JoinColumn(name = "Outfit_id"), 
			inverseJoinColumns = @JoinColumn(name = "OutfitItem_id"), 
			name = "Outfit_has_OutfitItem")
	public List<OutfitItem> getItems() {
		return items;
	}

	public void setItems(List<OutfitItem> items) {
		this.items = items;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
}
