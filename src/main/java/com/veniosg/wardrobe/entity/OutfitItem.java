package com.veniosg.wardrobe.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class OutfitItem {
    private Long id;
    private Float xCoord;
    private Float yCoord;
    private Float scale;
    private Float rotation;
    private Garment garment;

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "xCoord")
	public Float getxCoord() {
		return xCoord;
	}

	public void setxCoord(Float xCoord) {
		this.xCoord = xCoord;
	}

	@Column(name = "yCoord")
	public Float getyCoord() {
		return yCoord;
	}

	public void setyCoord(Float yCoord) {
		this.yCoord = yCoord;
	}

	public Float getScale() {
		return scale;
	}

	public void setScale(Float scale) {
		this.scale = scale;
	}

	public Float getRotation() {
		return rotation;
	}

	public void setRotation(Float rotation) {
		this.rotation = rotation;
	}

	// For extra fun, see: http://tai-dev.blog.co.uk/2011/11/18/fun-with-hibernates-cascadetype-persist-and-cascadetype-all-the-case-of-the-unsaved-transient-instance-12187037/#c18886678
    @ManyToOne(fetch = FetchType.EAGER,
			optional = false)
    @JoinColumn(name = "Garment_id")
	public Garment getGarment() {
		return garment;
	}

	public void setGarment(Garment garment) {
		this.garment = garment;
	}
}
