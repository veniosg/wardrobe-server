package com.veniosg.wardrobe.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Type {
	private Long id;
	private String title;
	private Integer allowedInstancesInOutfit;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getAllowedInstancesInOutfit() {
		return allowedInstancesInOutfit;
	}

	public void setAllowedInstancesInOutfit(Integer allowedInstancesInOutfit) {
		this.allowedInstancesInOutfit = allowedInstancesInOutfit;
	}
}
