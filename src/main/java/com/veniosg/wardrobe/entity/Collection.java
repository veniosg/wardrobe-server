package com.veniosg.wardrobe.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class Collection {
    private Long id;
    private String title;
    private List<Garment> garments;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	@ManyToMany()
	@LazyCollection(LazyCollectionOption.FALSE)		// See http://stackoverflow.com/questions/10769656/how-to-use-jpa-2-0-manytomany-without-issues
	@JoinTable(
			name = "Collection_has_Garment",
			joinColumns=@JoinColumn(name = "Collection_id"),
			inverseJoinColumns=@JoinColumn(name = "Garment_id"))
	public List<Garment> getGarments() {
		return garments;
	}

	public void setGarments(List<Garment> garments) {
		this.garments = garments;
	}
}
