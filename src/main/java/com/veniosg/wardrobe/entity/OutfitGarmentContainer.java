package com.veniosg.wardrobe.entity;

import java.util.ArrayList;
import java.util.Random;

import com.veniosg.wardrobe.util.color.ColorMatchingUtils;

public class OutfitGarmentContainer {
	public static final int MIN_NUM_ITEMS = 4;
	
	private Random random = new Random();
	private int maxItems = random.nextInt(4) + MIN_NUM_ITEMS; // random int in [MIN_NUM_ITEMS, MIN_NUM_ITEMS + 4)
	private ArrayList<Garment> garments = new ArrayList<Garment>();

	/**
	 * 
	 * @param name A name for the outfit. If null a default name will be used.
	 * @return
	 */
	public Outfit toNewOutfit(String name) {
		ArrayList<OutfitItem> items = new ArrayList<OutfitItem>();
		
		for(Garment g : garments) {
			// Create outfitItem containing garment
			OutfitItem item = new OutfitItem();
			item.setGarment(g);
			
			// Position item
			item.setxCoord(randomCoord());
			item.setyCoord(randomCoord());
			item.setScale(0.5F);
			item.setRotation(0F);
			
			// Add it
			items.add(item);
		}
		
		Outfit outfit = new Outfit();
		outfit.setItems(items);
		outfit.setTitle(name == null ? "Generated outfit" : name);
		return outfit;
	}
	
	public OutfitGarmentContainer() {
	}

	/**
	 * @return A random value in the range accepted by 
	 * {@link OutfitItem#setxCoord(Float)} and {@link OutfitItem#setyCoord(Float)}
	 */
	private Float randomCoord() {
		return random.nextFloat() * 0.6F + 0.2F;	// Actually random in [0.2, 0.8)
	}

	public ArrayList<Garment> getGarments() {
		return garments;
	}
	
	public boolean isEmpty() {
		return garments.isEmpty();
	}
	
	/**
	 * @param colorToCheck Color of the item-to-add.
	 * @return Returns true if the color passed matches the colors of the garments already included.
	 */
	public boolean colorMatches(int colorToCheck) {
		boolean matches = true;
		
		for (Garment g : garments) {
			matches &= ColorMatchingUtils.doGarmentColorsMatch(colorToCheck, g.getPicture().getColor());
		}
		
		return matches;
	}
	
	/**
	 * Check whether this instance is allowed to include an additional 
	 * garment of type t, or the limit of instances has been reached.
	 * @param t The type whose limit to check.
	 * @return True if you should <b>not</b> add any more garments of type t.
	 */
	public boolean hasAllAllowedInstancesOfType(Type t) {
		int numOfTypeInstances = 0;
		
		// Count type instances
		for(Garment g : garments) {
			if (g.getType().getId().equals(t.getId())) {
				numOfTypeInstances++;
			}
		}
		
		return numOfTypeInstances >= t.getAllowedInstancesInOutfit();
	}
	
	public boolean hasGarmentOfType(Type t) {
		for(Garment g : garments) {
			if (g.getType().getId().equals(t.getId())) {
				return true;
			}
		}
		
		return false;
	}
	
	public int getMaxItems() {
		return maxItems;
	}
	
	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}
}