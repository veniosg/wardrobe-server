package com.veniosg.wardrobe.entity;


public class ImageUploadResponse {
	private String path;
	
	public ImageUploadResponse(String path) {
		this.path = path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getPath(){
		return path;
	}
}
