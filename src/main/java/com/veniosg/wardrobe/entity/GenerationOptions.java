package com.veniosg.wardrobe.entity;

import com.veniosg.wardrobe.entity.Garment.Weather;

/**
 * Parameters for the call to outfit/generated.
 */
public class GenerationOptions {
	private Weather weather;
	private int maxItems;
	
	public Weather getWeather() {
		return weather;
	}
	public void setWeather(Weather weather) {
		this.weather = weather;
	}
	public int getMaxItems() {
		return maxItems;
	}
	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}
}
