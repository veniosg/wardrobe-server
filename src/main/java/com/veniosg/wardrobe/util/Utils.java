package com.veniosg.wardrobe.util;

import java.util.ArrayList;
import java.util.Collection;


public final class Utils {
	private Utils(){}
	
	/**
	 * Combine objects of any type in a single {@link Iterable} instance. <br/>
	 * Note: as this is meant to be used primarily for fact insertion, 
	 * any passed Collection instance will be divided and its children added individually
	 * in the returned Iterable.
	 * @param objects The items to be contained in the result.
	 * @return An Iterable instance containing all objects passed through objects.
	 */
	public static Iterable<Object> combineToIterable(Object... objects) {
		ArrayList<Object> result = new ArrayList<Object>();
		
		for(Object ob : objects) {
			if(ob instanceof Collection) {
				for(Object obChild : (Collection<?>) ob) {
					result.add(obChild);
				}
			} else {
				result.add(result);
			}
		}
		
		return result;
	}
	
	public static float degreesToPercentage(int degrees) {
		return (float) degrees / 360F;
	}
	
	public static <E> Collection<E> iterableToCollection(Iterable<E> iter) {
		Collection<E> list = new ArrayList<E>();
		for (E item : iter) {
			list.add(item);
		}
		return list;
	}
}
