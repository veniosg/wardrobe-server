package com.veniosg.wardrobe.drools;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.kie.api.runtime.rule.AccumulateFunction;

/**
* An accumulate function that randomly selects one object from a list of them.
*/
public class RandomSelectAccumulateFunction implements AccumulateFunction {

	public RandomSelectAccumulateFunction() {
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException {
	}

	@Override
	public void accumulate(Serializable context, Object value) {
		RandomSelectData data = (RandomSelectData) context;
	    data.list.add(value);
	}

	@Override
	public Serializable createContext() {
		return new RandomSelectData();
	}

	@Override
	public Object getResult(Serializable context) throws Exception {
		RandomSelectData data = (RandomSelectData) context;
		
		if(data.list.size() <= 0) {
			return null;
		}
		
		return data.list.get(data.random.nextInt(data.list.size()));
	}

	@Override
	public Class<?> getResultType() {
		return null;
	}

	@Override
	public void init(Serializable context) throws Exception {
		RandomSelectData data = (RandomSelectData) context;
		data.list.clear();
	}

	@Override
	public void reverse(Serializable context, Object value) throws Exception {
		RandomSelectData data = (RandomSelectData) context;
	    data.list.remove(value);
	}

	@Override
	public boolean supportsReverse() {
		return true;
	}
	
	/**
	  * A private static class to hold all the rule specific data for the random select function
	  */
	@SuppressWarnings("serial")
	private static class RandomSelectData implements Serializable {
		/** List of objects to choose from */
	    public List<Object> list = new ArrayList<Object>();
	    
	    /** Random number generator */
	    public transient Random random = new Random(System.currentTimeMillis());
	}

}
