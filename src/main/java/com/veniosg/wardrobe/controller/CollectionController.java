package com.veniosg.wardrobe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.veniosg.wardrobe.entity.Collection;
import com.veniosg.wardrobe.repository.CollectionRepository;
import com.veniosg.wardrobe.repository.GarmentRepository;
import com.veniosg.wardrobe.util.Utils;

@Controller
public class CollectionController {
	@Autowired
	private CollectionRepository collectionRepository;
	@Autowired
	private GarmentRepository garmentRepository;
	
	@RequestMapping(value = "collection",
			method = RequestMethod.POST,
			produces = "application/json"
			)
	@ResponseBody
	public Collection create(
			@RequestBody Collection collection, 
			Model model) {
		return collectionRepository.save(collection);
	}
	
	@RequestMapping(value = "collection/{id}", 
			method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void update(
			@RequestBody Collection collection,
			Model model) {
		collectionRepository.save(collection);
	}

	@RequestMapping(value = "collection/{id}", 
			method = RequestMethod.GET, 
			produces = "application/json")
	@ResponseBody
	public Collection read(
			@PathVariable Long id,
			Model model) {
		return collectionRepository.findOne(id);
	}

	@RequestMapping(value = "collection/{id}", 
			method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(
			@PathVariable Long id,
			Model model) {
		collectionRepository.delete(id);
	}
	
	@RequestMapping(value = "collection", 
			method = RequestMethod.GET)
	@ResponseBody
	public java.util.Collection<Collection> list() {
		return Utils.iterableToCollection(collectionRepository.findAll());
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleClientErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
 
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleServerErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
}