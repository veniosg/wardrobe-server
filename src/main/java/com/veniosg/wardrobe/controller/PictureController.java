package com.veniosg.wardrobe.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.veniosg.wardrobe.entity.Picture;
import com.veniosg.wardrobe.repository.PictureRepository;
import com.veniosg.wardrobe.util.Utils;

@Controller
public class PictureController {
	@Autowired
	private PictureRepository pictureRepository;
	
	@RequestMapping(value = "picture", 
			method = RequestMethod.POST,
			produces = "application/json"
			)
	@ResponseBody
	public Picture create(
			@RequestBody Picture Picture, 
			Model model) {
		return pictureRepository.save(Picture);
	}
	
	@RequestMapping(value = "picture/{id}", 
			method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void update(
			@RequestBody Picture Picture,
			Model model) {
		pictureRepository.save(Picture);
	}

	@RequestMapping(value = "picture/{id}", 
			method = RequestMethod.GET, 
			produces = "application/json")
	@ResponseBody
	public Picture read(
			@PathVariable Long id,
			Model model) {
		return pictureRepository.findOne(id);
	}

	@RequestMapping(value = "picture/{id}", 
			method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(
			@PathVariable Long id,
			Model model) {
		pictureRepository.delete(id);
	}
	
	@RequestMapping(value = "picture", 
			method = RequestMethod.GET)
	@ResponseBody
	public Collection<Picture> list() {
		return Utils.iterableToCollection(
				pictureRepository.findAll());
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleClientErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
 
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleServerErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
}