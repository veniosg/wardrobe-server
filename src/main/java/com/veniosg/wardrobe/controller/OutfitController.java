package com.veniosg.wardrobe.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.kie.api.command.Command;
import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.veniosg.wardrobe.entity.Garment.Weather;
import com.veniosg.wardrobe.entity.GenerationOptions;
import com.veniosg.wardrobe.entity.Outfit;
import com.veniosg.wardrobe.entity.OutfitGarmentContainer;
import com.veniosg.wardrobe.entity.OutfitItem;
import com.veniosg.wardrobe.repository.GarmentRepository;
import com.veniosg.wardrobe.repository.OutfitRepository;
import com.veniosg.wardrobe.repository.TypeRepository;
import com.veniosg.wardrobe.util.Utils;

@Controller
public class OutfitController {
	private static final Long PANTS_TYPE_ID = 1L;
	private static final Long DRESS_TYPE_ID = 2L;
	
	@Autowired
	private OutfitRepository outfitRepository;
	
	@Autowired
	private StatelessKieSession knowledgeSession;
	@Autowired
	private GarmentRepository garmentRepository;
	@Autowired
	private TypeRepository typeRepository;

	/**
	 * Generate a new outfit based on the passed parameters and the persisted clothes.
	 * @param optsJson The JSON representation of a {@link OutfitGarmentContainer} instance. See below for details on its fields.
	 * 
	 * <p>
	 * <b>weather</b> The weather for which to produce the outfit.
	 * </p>
	 * <p>
	 * <b>maxItems</b> The max number of items this outfit will include. 
	 * Anything below {@link OutfitGarmentContainer#MIN_NUM_ITEMS} will be ignored.
	 * </p>
	 * 
	 * @return The generated, non-persisted outfit.
	 */
	@RequestMapping(value = "outfit/generated",
			method = RequestMethod.GET)
	@ResponseBody
	public Outfit generate(@RequestParam(required = false, value = "opts", defaultValue = "") String optsJson) 
			throws JsonParseException, JsonMappingException, IOException {
		GenerationOptions opts;
		if (optsJson != null && !optsJson.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			opts = mapper.readValue(optsJson, GenerationOptions.class);
		} else {
			opts = new GenerationOptions();
		}
		DefaultAgendaEventListener logger = new DefaultAgendaEventListener() {
			@Override
			public void afterMatchFired(AfterMatchFiredEvent event) {
				System.out.println("Drools: Rule " + event.getMatch().getRule().getName() + " fired!");
			}
		};
		
		// Init generation options
		OutfitGarmentContainer outfitGarmentContainer = new OutfitGarmentContainer();
		if(opts.getMaxItems() >= OutfitGarmentContainer.MIN_NUM_ITEMS) {
			outfitGarmentContainer.setMaxItems(opts.getMaxItems());
		}
		if(opts.getWeather() == null) {
			opts.setWeather(Weather.ANY);
		}

		knowledgeSession.addEventListener(logger);
		
		// Init working memory
		List<Command<?>> commands = new ArrayList<Command<?>>();
		commands.add(CommandFactory.newInsertElements(Utils.iterableToCollection(
															garmentRepository.findAll())));
		commands.add(CommandFactory.newInsertElements(Utils.iterableToCollection(
															typeRepository.findAll())));
		commands.add(CommandFactory.newInsert(outfitGarmentContainer));
		commands.add(CommandFactory.newInsert(opts.getWeather()));
		// Randomly (uniformly) choose whether to add a dress if one exists.
		if(shouldAddDress()) {
			commands.add(CommandFactory.newInsert(new ContainDress()));
		}
		
		knowledgeSession.execute(CommandFactory.newBatchExecution(commands));
		
		knowledgeSession.removeEventListener(logger);
		
		// Not using globals for OutfitGarmentContainer in WM because Drools docs state: 
		// "Globals are not inserted into the Working Memory, and therefore a global 
		// should never be used to establish conditions in rules except when it has 
		// a constant immutable value"
		Outfit outfit = outfitGarmentContainer.toNewOutfit(null);
		return outfit;	// Does not persist. Client will have to do a "create" call.
	}
	
	@RequestMapping(value = "outfit", 
			method = RequestMethod.POST,
			produces = "application/json"
			)
	@ResponseBody
	public Outfit create(
			@RequestBody Outfit outfit, 
			Model model) {
		// Drop isNewlyBought flag from garments existing in an accepted outfit.
		for(OutfitItem item : outfit.getItems()) {
			if(item.getGarment().getRecentlyPurchased()) {
				item.getGarment().setRecentlyPurchased(Boolean.FALSE);
				
				garmentRepository.save(item.getGarment());
			}
		}
		
		return outfitRepository.save(outfit);
	}
	
	@RequestMapping(value = "outfit/{id}", 
			method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void update(
			@RequestBody Outfit outfit,
			Model model) {
		outfitRepository.save(outfit);
	}

	@RequestMapping(value = "outfit/{id}", 
			method = RequestMethod.GET, 
			produces = "application/json")
	@ResponseBody
	public Outfit read(
			@PathVariable Long id,
			Model model) {
		return outfitRepository.findOne(id);
	}

	@RequestMapping(value = "outfit/{id}", 
			method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(
			@PathVariable Long id,
			Model model) {
		outfitRepository.delete(id);
	}
	
	@RequestMapping(value = "outfit", 
			method = RequestMethod.GET)
	@ResponseBody
	public Collection<Outfit> list() {
		return Utils.iterableToCollection(
				outfitRepository.findAll());
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleClientErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
 
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleServerErrors(Exception ex) {
    	ex.printStackTrace();
        return ex.getMessage();
    }
	
    /**
     * Ensures uniform distribution amongst all pants+dresses.
     * @return True to allow adding a dress, false otherwise.
     */
	private boolean shouldAddDress() {
		Random r = new Random();
		int dressCount = garmentRepository.findByType_Id(DRESS_TYPE_ID).size();
		int pantCount = garmentRepository.findByType_Id(PANTS_TYPE_ID).size();
		float threshold = r.nextFloat();
		float dressPercentage = dressCount + pantCount > 0 ? 
				(float) dressCount / (float) (dressCount + pantCount)
				: 0;
				
		return dressPercentage >= threshold;
	}
    
    /**
     * Control fact class.
     */
    public static class ContainDress {}
}