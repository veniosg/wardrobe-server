package com.veniosg.wardrobe.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.veniosg.wardrobe.entity.Garment;
import com.veniosg.wardrobe.repository.GarmentRepository;
import com.veniosg.wardrobe.util.Utils;

@Controller
public class GarmentController {
	@Autowired
	private GarmentRepository garmentRepository;
	
	@RequestMapping(value = "garment", 
			method = RequestMethod.POST,
			produces = "application/json"
			)
	@ResponseBody
	public Garment create(
			@RequestBody Garment garment, 
			Model model) {
		return garmentRepository.save(garment);
	}
	
	@RequestMapping(value = "garment/{id}", 
			method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void update(
			@RequestBody Garment garment,
			Model model) {
		garmentRepository.save(garment);
	}

	@RequestMapping(value = "garment/{id}", 
			method = RequestMethod.GET, 
			produces = "application/json")
	@ResponseBody
	public Garment read(
			@PathVariable Long id,
			Model model) {
		return garmentRepository.findOne(id);
	}

	@RequestMapping(value = "garment/{id}", 
			method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(
			@PathVariable Long id,
			Model model) {
		garmentRepository.delete(id);
	}
	
	@RequestMapping(value = "garment", 
			method = RequestMethod.GET)
	@ResponseBody
	public Collection<Garment> list() {
		return Utils.iterableToCollection(garmentRepository.findAll());
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleClientErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
 
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleServerErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
}