package com.veniosg.wardrobe.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.veniosg.wardrobe.entity.OutfitItem;
import com.veniosg.wardrobe.repository.OutfitItemRepository;
import com.veniosg.wardrobe.util.Utils;

@Controller
public class OutfitItemController {
	@Autowired
	private OutfitItemRepository outfitItemRepository;
	
	@RequestMapping(value = "outfitItem", 
			method = RequestMethod.POST,
			produces = "application/json"
			)
	@ResponseBody
	public OutfitItem create(
			@RequestBody OutfitItem outfitItem, 
			Model model) {
		return outfitItemRepository.save(outfitItem);
	}
	
	@RequestMapping(value = "outfitItem/{id}", 
			method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void update(
			@RequestBody OutfitItem outfitItem,
			Model model) {
		outfitItemRepository.save(outfitItem);
	}

	@RequestMapping(value = "outfitItem/{id}", 
			method = RequestMethod.GET, 
			produces = "application/json")
	@ResponseBody
	public OutfitItem read(
			@PathVariable Long id,
			Model model) {
		return outfitItemRepository.findOne(id);
	}

	@RequestMapping(value = "outfitItem/{id}", 
			method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(
			@PathVariable Long id,
			Model model) {
		outfitItemRepository.delete(id);
	}
	
	@RequestMapping(value = "outfitItem", 
			method = RequestMethod.GET)
	@ResponseBody
	public Collection<OutfitItem> list() {
		return Utils.iterableToCollection(
				outfitItemRepository.findAll());
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleClientErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
 
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleServerErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
}