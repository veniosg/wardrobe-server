package com.veniosg.wardrobe.controller;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

import com.veniosg.wardrobe.entity.ImageUploadResponse;

@Controller
public class ImageController {
	@Autowired
	private ServletContext applicationContext;
	
	@RequestMapping(value = "/picture/image", 
			method = RequestMethod.POST, 
			produces = "application/json")
	@ResponseBody
	public ImageUploadResponse upload(
			@RequestParam("image") MultipartFile image, 
			Model model) throws IOException {
		if(!image.isEmpty()) {
			int lastDotIndex = image.getOriginalFilename().lastIndexOf('.');	// Only works for single-dotted extensions. 
																				// Should suffice for our image uploading needs..
			String originalExtension;
			if(lastDotIndex != -1) {
				originalExtension = image.getOriginalFilename().substring(lastDotIndex);
			} else {
				originalExtension = "";
			}
			String resourceFilename = new Date().getTime() + originalExtension;
			StringBuilder parentDirPathBuilder = new StringBuilder(
					applicationContext.getRealPath(applicationContext.getContextPath()));
			parentDirPathBuilder.append("/resources/images/");
			File parentFile = new File(parentDirPathBuilder.toString());
			File saveTo = new File(parentFile, resourceFilename);
			
			if(!parentFile.exists()) {
				parentFile.mkdirs();
			}
			
			if(parentFile.exists() && parentFile.isDirectory() && saveTo.createNewFile()) {
				// Write the file to the FS
				image.transferTo(saveTo);
			}
			
			return new ImageUploadResponse("/resources/images/" + saveTo.getName());	// Hard-coding FTW
		} else {
			throw new IllegalArgumentException("Uploaded image was empty!");
		}
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleClientErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
 
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleServerErrors(Exception ex) {
		ex.printStackTrace();
        return ex.getMessage();
    }
}